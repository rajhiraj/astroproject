import React,{useState} from "react";
import Slider from "react-slick";
import '../scss/App.scss';
import '../scss/font.scss';
import '../scss/blog.scss';
import '../scss/about.scss';
import {Card,Button,Container,Form,Col,InputGroup,FormControl,Modal,Breadcrumb} from 'react-bootstrap'
// import { MDBCol, MDBFormInline, MDBIcon } from "mdbreact";
import Navbar from '../components/Navbar';
import Footer from '../components/Footer';
import blog1 from "../images/blog/blog1.jpeg";
import search from "../images/search.png";
import fb from "../images/facebook.png";
import wp from "../images/whatsapp.png";
import yb from "../images/youtube.png";
import insta from "../images/instagram.png";
import calendar from "../images/schedule.png";
import comment from "../images/comments.png";

function Blogs() {
   const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  return (
      <>
      <Navbar />
       <div className="header_image">
           {/* <span >Blog</span> */}
           <Breadcrumb className="header_text">
  <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
  <Breadcrumb.Item href="/blog">
   Our Blogs
  </Breadcrumb.Item>
  <Breadcrumb.Item active>Mundane Astrology </Breadcrumb.Item>
</Breadcrumb>
        </div>
    <div className="blogContainer" id="blog">

        <div className="left">
             <InputGroup className="mb-2 mr-sm-2">
              <InputGroup.Prepend>
                <InputGroup.Text><img src={search} alt="search" style={{width:15,height:15}} /></InputGroup.Text>
              </InputGroup.Prepend>
              <FormControl id="inlineFormInputGroupUsername2" placeholder="Search" />
            </InputGroup>
            <div className="category">
              <h5>CATEGORIES</h5>
              <p className="categoris">Gemstone (504)</p>
              <p className="categoris">Medical Astrology (79)</p>
              <p className="categoris">Astrology (345)</p>
              <p className="categoris">Rudraksha (69)</p>
              <p className="categoris">Horoscope (61)</p>
            </div>
          
          
            {/* <h4 className="blogHeading">GET FREE CONSULTATION</h4> */}
             <Button variant="primary" onClick={handleShow} style={{marginTop:'20%'}}>
       GET FREE CONSULTATION
      </Button>
       <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Get Free Consultent</Modal.Title>
        </Modal.Header>
        <Modal.Body>
                 <Form>
                 <Form.Group controlId="formBasicEmail">
    <Form.Control type="text" placeholder="Name" />
  </Form.Group>
  <Form.Group controlId="formBasicEmail">
    <Form.Control type="email" placeholder="Email" />
  </Form.Group>
    <Form.Group controlId="formBasicEmail">
    <Form.Control type="text" placeholder="Mobile No." />
  </Form.Group>
    <Form.Group controlId="formBasicEmail">
    <Form.Control type="text" placeholder="Gender" />
  </Form.Group>
    <Form.Group controlId="formBasicEmail">
    <Form.Control type="text" placeholder="Date of Birth" />
  </Form.Group>
    <Form.Group controlId="formBasicEmail">
    <Form.Control type="text" placeholder="Birth Place" />
  </Form.Group>
    <Form.Group controlId="formBasicEmail">
    <Form.Control type="text" placeholder="Birth Hour" />
  </Form.Group>
  <Form.Group controlId="formBasicEmail">
    <Form.Control type="text" placeholder="Birth Minute" />
  </Form.Group>
    <Form.Group controlId="formBasicEmail">
    <Form.Control type="text" placeholder="Birth Second" />
  </Form.Group>
    <Form.Group controlId="formBasicEmail">
    <Form.Control type="textarea" placeholder="Your Concern" />
  </Form.Group>
</Form>

        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={handleClose}>
            Submit
          </Button>
        </Modal.Footer>
      </Modal>

       
        </div>
        <div className="right">
            <img src={blog1} alt="blog1" className="blog1"/>
            <h1 className="blogHeading">Saturn Retrograde 2021</h1>
             <div className="social" style={{justifyContent:'flex-start',marginBottom:'2%',marginTop:'3%'}}>
      <div className="calendar">
        <img src={calendar} alt="calendar" />
    <Card.Text style={{color:'blue'}} className="blog_content">Jan 9, 2019</Card.Text>
    </div>
     <div className="comment" style={{marginLeft:'5%'}}>
        <img src={comment} alt="comment" />
    <Card.Text style={{color:'blue'}} className="blog_content">0</Card.Text>
    </div>
    </div>
            <p>On 23rd May 2021, the planet Saturn will be retrograded in Capricorn itself. It is seen that whenever planet Jupiter and Saturn transits, it brings some massive changes on earth as well as on mankind. Saturn retrogression in Capricorn always brings lots of changes like changes of generation It is evident, as we are witnessing since November 19, 2020 onwards our difficulties and struggle have increased. Now let us see how this Saturn transit will bring changes. Whenever Mr. Prashant Kapoor predicts about any kind of natural calamity, he does so on the basis of his study of planetary positions. He makes an attempt to update environmentalists so that they are ready with some counter actions and save mankind from destruction. His study of planetary positions indicates that there will be big treaties and trade agreements on an international level which will bring constructive results for India. But at the same time the analysis indicates a massive earthquake in India and other parts of the world, which will lead to lose and destruction of mankind, worldwide communication failure. This Saturn transition will continue till 11 October and till that time these kind mishaps will occur from time to time.  Online fraud will increase, banks will face turbulence, virus mutation will be faster than before. It also shows after 14 September 2021 the third wave of Covid will hit. In his earlier video #khapparYoga the astrologer has explained his analysis in detail.</p>
            <p>Kurma chakra indicates immediate changes worldwide – good bad both. It also predicts war, internal fights, and China’s attack on Arunachal Pradesh. The cases of terrorism will spike in India or outside India, lack of food or high cost of some categories of food will bring new challenge for people. Some places in India like Ganganagar, Bhatinda, Amritsar, Jodhpur, Bikaner, some parts of Gujarat, coastal areas, Kullu Manali, Ladakh will face massive destruction. Along with these states Pakistan, Islamabad, Lahore, Sialkot, Muzaffarabad, Kabul, Afghanistan will be highly affected areas. Canada, Gandhar and some parts of the USA also will face major harm to mankind. Earthquake, epidemic will bring innumerable challenges for people.  Professionals associated with divine science will become victims of state or central govt. High police officials will face cases. There will be earthquakes and volcanic eruptions in cold regions. With this Mr. Prashant Kapoor is sending a message to all his viewers as upcoming times seem to be challenging for everyone, he is requesting everyone to stay strong and maintain your immunity high.</p>
           <div style={{display:'flex',flexDirection:'row',marginRight:'10px'}}>
            <p>SHARE THIS ON:</p>
              <div className="icon">
    <a href=""><img src={fb} alt="fb" /></a>
     <a href=""><img src={wp} alt="wp" /></a>
      <a href=""><img src={insta} alt="insta" /></a>
     <a href=""><img src={yb} alt="yb" /></a>
    </div>
    </div>
            <p>POSTED IN: Mundane Astrology</p>
            <h2 className="blogHeading">LEAVE A REPLY</h2>
             <Form>
                   <Form.Group controlId="formBasicEmail">
    <Form.Control type="textarea" placeholder="Comment" />
  </Form.Group>
                 <Form.Group controlId="formBasicEmail">
                      <Form.Row>
    <Col>
      <Form.Control type="text" placeholder="Name" />
    </Col>
    <Col>
      <Form.Control type="email" placeholder="Email" />
    </Col>
  </Form.Row>
   
  </Form.Group>
   <Form.Group controlId="formBasicEmail">
    <Form.Control type="text" placeholder="Website" />
  </Form.Group>
     <Form.Group controlId="formBasicEmail">
     <Form.Label className="blogHeading">18 − sixteen =</Form.Label>
    <Form.Control type="text" />
  </Form.Group>
  <Button variant="primary" type="submit">
    Post Comment
  </Button>
</Form>
       
        </div>
        <div className="middle">
           <div className="category" style={{marginTop:'0%'}}>
              <h5>RECENT POSTS</h5>
              <p className="categoris">Gemstone (504)</p>
              <p className="categoris">Medical Astrology (79)</p>
              <p className="categoris">Astrology (345)</p>
              <p className="categoris">Rudraksha (69)</p>
              <p className="categoris">Horoscope (61)</p>
            </div>
           <div className="category">
              <h5>RECENT COMMENTS</h5>
              <p className="categoris">Gemstone (504)</p>
              <p className="categoris">Medical Astrology (79)</p>
              <p className="categoris">Astrology (345)</p>
              <p className="categoris">Rudraksha (69)</p>
              <p className="categoris">Horoscope (61)</p>
            </div>
          
        </div>
    </div>
    <Footer />
    </>
 );
}

export default Blogs;
