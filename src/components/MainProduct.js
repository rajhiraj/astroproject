import React from "react";
import {Breadcrumb,Form,Col,Button} from 'react-bootstrap'
import Navbar from '../components/Navbar';
import Footer from '../components/Footer';
import '../scss/product.scss';
import main from '../images/product/main.jpg';
import product1 from '../images/product/product1.jpg';
import product2 from '../images/product/product2.jpg';
import product3 from '../images/product/product3.jpg';
import product4 from '../images/product/product4.jpg';
function MainProduct() {
  return (
  <>
    <Navbar />
    <div className="mainProduct">
        <img src={main} alt="main" style={{marginBottom:'5%',marginTop:'5%',width:'70%',height:350}}/>
        <h3 className="mainProductHeading">What are Gems?</h3>
        <p className="mainProductContent">Gems are also called as birthstones or lucky stones. Since the beginning of the civilization, Gems are thought to have mystic power. All the main various culture of Vedic, Egypt, Maya had used Gem enhancing healing powers and for the growth of mystical power. In our Vedic culture, Gems are prescribed after a complete analysis of horoscope and also by studying the effect of malefic and benefic planets in one’s horoscope.</p>
        <h3 className="mainProductHeading">Buy Gems Online – Helps to Impart Positive and Good Vibes</h3>
        <p className="mainProductContent">Gem has unique metaphysical properties, which helps to impart positive and good vibes to a concerned planet. There are nine planets (navagraha) according to Vedic and Western astrology. There are Sun (Surya), Moon (Chandra), Venus (Shukra), Jupiter (Guru), Mercury (Budh), Mars (Mangal), Saturn (Shani), Rahu and Ketu. In all the above mentioned Sun is not a planet, it is a star and Rahu and Ketu are only the nodes.</p>
        <h3 className="mainProductHeading">Buy Certified Gems Online – They are Strong Influencers to Your Concerned Graha</h3>
        <p className="mainProductContent">Each Gem is associated with the nine planets and imparts very strong influence on the concerned Graha. Associated with the lord Sun is Ruby (Manikya), with the Lord Moon is Pearl (Moti), with the lord Saturn is Blue sapphire (Neelam), with the lord Jupiter is Yellow sapphire (Pukhraaj), with the lord Mercury is Emerald(Panna), with lord Mars is Red coral (Moonga), with the lord Rahu is Hessonite (Gomedh) and with lord Ketu is Cat’s eye (Lehsunia). All these Gems are precious Gem that are used to ward of evils and contains excellent metaphysical properties. They are known to boost the spiritual power of the body by keeping the people happy and healthy.</p>
        <h3 className="mainProductHeading">Buy Certified Gems Online in Wholesale Price</h3>
        <p className="mainProductContent">These are very pretty stones that can be polished and word to ward off negative energy. Gem which stimulate powerful and positive radiation in the body are mostly made from minerals. But very precious stones like pearls and amber are made from living organism. They are shaped, carved, cut and polished according to the liking. These Gems are worn by the people very close to their skin to ward off evil effect and remove the obstacle of their life. For example, as Ruby is the stone of the Sun, so it can be used to remove the malefic effect of Sun. Ruby is available in red, yellow, and in rose color.</p>
        <p className="mainProductContent">Gem work with both kind of energy- physiochemical i.e. electrochemical and pranic representing the vital force. The Puranas furnish wonderful stories describing the excellent benefic of this beautiful Gemtones. According to Karma Puranas, Gem were created from the seven rays radiating from the seven foremost planets of the solar system. These rays were transmitted as seven colours in the rainbow.</p>
        <p className="mainProductContent">The chemical nature of the Gem brings them in very close proximity to human beings. These are also called as crystals of clear and purified chemicals that are also present in the human body. Their contact with the electromagnetic field of the body becomes easier when they are studded in pure electrolytes like silver, copper and gold.</p>
        <p className="mainProductContent">Gems are always considered as a garland of happiness, peace and prosperity. These act as a store for celestial energy and are also used as sculpturing object of worship</p>
        <p className="mainProductContent">But according to Vedic scriptures, the effect of particular Gem completely depends upon the person’s horoscope and the planetary positions in the birth chart. Gem which are favourable for you is considered as Anukul, which will bring name, fame, prosperity, power and will make your life full of positive bliss. It enhances good energy in all spheres of life. It helps to attain a good height in your career, profession, education, financial gains and status. Gems are excellent in removing negative energies that produce suffering and losses in life.</p>
        <p className="mainProductContent">Since wearing of Gem can bring negative tendencies in the life, so we never advise our clients for self-experimentation. We at Astrokapoor group have Astrologer Prashant Kapoor who guide and analyze one’s horoscope and give correct advice for Gem according to one’s need. At Astrokapoor we take special precaution to stabilize harmony in one’s life, so very special care is taken for prescribing Gemtones</p>
        <p className="mainProductContent">since all the Astro Vedic Gem works on the principle of particular Graha and particular wavelength, they can be used in many ways.</p>
        <p className="mainProductContent">On the basis of one’s horoscope and, malefic and benefic planets Gems are decided to strengthen them. These Gems are worn particularly by touching the skin.</p>
        <p className="mainProductContent">If you have adverse planets in your birth chart, then Astro Vedic Gems are used to ward off evil effects of the particular Planets/Grahas.</p>
        <p className="mainProductContent">These Gems are also used in meditation for spiritual attainment.</p>
        <p className="mainProductContent">Gems are also used for adding cosmic colour to one’s own aura. When a person is born on the earth, then constellations, signs, and as well as the planets imparts some negative and positive effect on that person as they attract different energy centres present in the body. This forms an aura around the person and it stays with him the rest of his life. This aura keeps on changing and cause sometimes positive and sometimes negative effect on a person’s life. Gems are excellent for balancing the aura and create positive vibes in the life.</p>
        <p className="mainProductContent">Click here to <a>Buy online all kind of Natural Certified Precious and Semi Precious Gemstones </a> in Wholesale Prices from In-house Manufacturing of AstroKapoor</p>
    </div>
   
    <Footer />
  </>
 );
}

export default MainProduct;
